const serialize = (obj, path = '', finalObj = {}) => {
  if(Array.isArray(obj)) {
   return obj.reduce((acc, ele, index) =>
      ({ ...acc, ...serialize(ele, path.concat(`${index}_`), acc) }), finalObj)
  }

  return Object.entries(obj).reduce((acc, [name, value]) => {      
    if(typeof value !== 'object') {
      return { ...acc, [path + name]: value }
    }
    if(!Array.isArray(value)) {
      return { ...acc, [path + name]: serialize(value) }
    }
    return { ...acc, ...serialize(value, path + name, acc) }
  }, {})
}

export default serialize