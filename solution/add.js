const add = (...args) => 
  args.reduce((sum, ele) => ele + sum, 0)

export default add