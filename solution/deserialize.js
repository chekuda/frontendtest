import moment from 'moment'

moment.locale('en-GB')

const deserialize = (obj, index, child = {}) => {
  if(Array.isArray(obj)) {
    const [childName, childValue] = Object.entries(child)[0]
    const currentChildValue = typeof childValue === 'string' && childValue.startsWith('t:')
      ? moment(parseInt(childValue.match(/\d+/)[0])).format('L')
      : childValue

    if(index > obj.length - 1) {
      return [...obj].concat({
        [childName]: typeof childValue === 'object'
        ? deserialize(childValue)
        : currentChildValue
      })
    }

    return obj.map((ele, childIndex) => childIndex == index
      ? ({ ...ele, [childName]: currentChildValue })
      : ele)
  }

  const sortedObject = Object.entries(obj).sort()
  return sortedObject.reduce((acc, [name, value]) => {
    const [currentName, childName] = name.split('_')
    const currentProp = currentName.match(/^[a-z]+/)[0]
    const newindex = (currentName.match(/\d+/) || [])[0]

    if(typeof newindex === 'string') {
      const currentChildValue = typeof value === 'object'
        ? deserialize(value)
        : typeof value === 'string' && value.startsWith('t:') ? moment(parseInt(value.match(/\d+/)[0])).format('L') : value

      return {
        ...acc,
        [currentProp]: (acc[currentProp] || []).length
          ? deserialize(acc[currentProp], newindex, { [childName]: currentChildValue })
          : [{ [childName]: currentChildValue }]
      }
    }
    return { ...acc, [currentProp]: value}
  }, {})

}

export default deserialize