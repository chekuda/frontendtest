const objectToList = (obj) =>
  Object.entries(obj).reduce((acc, [name, value]) => {
    
    return acc.concat({
      name,
      value: typeof value === 'object' ? JSON.parse(JSON.stringify(value)) : value
    })
  }, [])

export default objectToList
