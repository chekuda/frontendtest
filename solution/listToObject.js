const listToObject = (list) =>
  list.reduce((acc, { name, value }) => ({
    ...acc,
    [name]: typeof value === 'object' ? JSON.parse(JSON.stringify(value)) : value
  }), {})

export default listToObject